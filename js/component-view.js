(function ($, Drupal, drupalSettings, once) {
  Drupal.behaviors.zoomVideoBehavior = {
    attach(context, settings) {
      const client = ZoomMtgEmbedded.createClient();
      const zoomVideo = settings.zoom_video;
      const meetingSDKElement = document.getElementById("meetingSDKElement");
      const authEndpoint = "/zoom_video/endpoint";
      // Getting SDK Key from zoom_configuration_form's data.
      const sdkKey = zoomVideo.sdk_key;
      const {meetingNumber} = settings.zoom_video;
      const {password} = settings.zoom_video;
      const role = 0;

      function logInfo(info) {
        console.log(info);
      }

      function logError(error) {
        console.log("Error", error);
      }

      async function initializeAndJoinMeeting(signature, userName, userEmail) {
        try {
          await client.init({
            zoomAppRoot: meetingSDKElement,
            language: "en-US",
          });
          await client.join({
            sdkKey,
            signature,
            meetingNumber,
            userName,
            password,
            userEmail,
          });
          logInfo("joined successfully");
        } catch (error) {
          logError(error);
        }
      }

      function getSignature() {
        return fetch(authEndpoint, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({
            meetingNumber,
            role,
          }),
        })
          .then((response) => {
            logInfo(response);
            return response.json();
          })
          .catch((error) => {
            logError(error);
          });
      }

      $(once('zoomVideoWidget', '.zoom-meeting', context)).each(function () {
        const joinButton = $('.join-meeting', context);
        let userName = '';
        let userEmail = '';

        console.log(settings.user);
        if (settings.user.isAuthenticated) {
          // If user is authenticated, use the user's data from settings.
          userName = settings.user.userName;
          userEmail = settings.user.userEmail;
          joinButton.prop('disabled', false); // Enable button immediately.
        } else {
          // Enable/disable the join button based on input fields for anonymous users.
          $('#username, #email', context).on('input', function () {
            userName = $('#username').val().trim();
            userEmail = $('#email').val().trim();
            joinButton.prop('disabled', !(userName && userEmail));
          });
        }

        joinButton.click(async function () {
          if (userName && userEmail) {
            $(this).removeClass('hidden');
            const {signature} = await getSignature();
            await initializeAndJoinMeeting(signature, userName, userEmail);
          } else {
            alert('Please enter both your name and email.');
          }
        });
      });
    },
  };
})(jQuery, Drupal, drupalSettings, once);
