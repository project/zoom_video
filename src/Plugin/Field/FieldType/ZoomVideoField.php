<?php

namespace Drupal\zoom_video\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'zoom_video_field' field type.
 *
 * @FieldType(
 *   id = "zoom_video_field",
 *   label = @Translation("Zoom Video Field"),
 *   description = @Translation("Field type for storing Zoom video data."),
 *   default_widget = "zoom_video_widget",
 *   default_formatter = "zoom_video_formatter"
 * )
 */
class ZoomVideoField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'meeting_url' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'meeting_number' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'password' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(): void {
    $value = $this->getValue();
    preg_match('/\/j\/(\d+)\?pwd=([a-zA-Z0-9]+)/', $value['meeting_url'], $matches);
    if (count($matches) !== 3) {
      return;
    }
    $value['meeting_number'] = $matches[1];
    $value['password'] = $matches[2];
    $this->setValue($value);
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['meeting_url'] = DataDefinition::create('string')
      ->setLabel(t('Meeting Url'))
      ->setDescription(t('The Zoom meeting url.'));
    $properties['meeting_number'] = DataDefinition::create('string')
      ->setLabel(t('Meeting Number'))
      ->setDescription(t('The Zoom meeting number.'));
    $properties['password'] = DataDefinition::create('string')
      ->setLabel(t('Password'))
      ->setDescription(t('The Zoom meeting password.'));

    return $properties;
  }

}
