<?php

namespace Drupal\zoom_video\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'zoom_video_widget' widget.
 *
 * @FieldWidget(
 *   id = "zoom_video_widget",
 *   label = @Translation("Zoom Video Widget"),
 *   field_types = {
 *     "zoom_video_field"
 *   }
 * )
 */
class ZoomVideoWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {
    $meetingUrl = $items[$delta]->meeting_url ?? '';
    $element += [
      '#element_validate' => [[$this, 'validateMeetingUrl']],
    ];
    $element['#type'] = 'fieldset';
    $element['#title'] = $this->t('Zoom Video');

    $element['meeting_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Meeting URL'),
      '#default_value' => $meetingUrl,
    ];

    return $element;
  }

  /**
   * Validation function to extract meeting number and password from the URL.
   */
  public function validateMeetingUrl(array $element, FormStateInterface $form_state): void {
    $value = $element["meeting_url"]["#value"];
    // Skip validation for not required field and field type configuration.
    if (empty($value)) {
      return;
    }
    // Regular expression to extract meeting number and password from the URL.
    preg_match('/\/j\/(\d+)\?pwd=([a-zA-Z0-9]+)/', $value, $matches);

    if (count($matches) == 3) {
      // If the regular expression matched,
      // set the values for the meeting number and password.
      $form_state->setValueForElement($element['meeting_url'], $value);
    }
    else {
      // If the regular expression did not match, set an error.
      $form_state->setError($element, $this->t('Invalid Meeting URL. Please enter a valid Zoom Meeting URL.'));
    }
  }

}
