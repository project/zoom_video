<?php

namespace Drupal\zoom_video\Plugin\Field\FieldFormatter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'zoom_video_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "zoom_video_formatter",
 *   label = @Translation("Zoom Video Formatter"),
 *   field_types = {
 *     "zoom_video_field"
 *   }
 * )
 */
class ZoomVideoFormatter extends FormatterBase {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a ZoomVideoFormatter instance.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   Current user.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, ConfigFactoryInterface $config_factory, AccountProxyInterface $current_user) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->configFactory = $config_factory;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('config.factory'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $config = $this->configFactory->get('zoom_video.settings');
    $elements = [];

    foreach ($items as $delta => $item) {
      $meeting_number = $item->get('meeting_number')->getValue();
      $meeting_password = $item->get('password')->getValue();
      if ($meeting_number === NULL || $meeting_password === NULL) {
        continue;
      }

      $elements[$delta] = [
        '#theme' => 'zoom_video_formatter_template',
        '#meetingNumber' => $meeting_number,
        '#meetingPassword' => $meeting_password,
        '#attached' => [
          'library' => ['zoom_video/zoom-sdk-js-library'],
        ],
      ];
    }

    if (count($elements) > 0) {
      // Attach the zoom_video settings to drupalSettings.
      $elements['#attached']['drupalSettings']['zoom_video'] = [
        'sdk_key' => $config->get('sdk_key'),
        // @todo We need to improve related JS code to handle multiple values.
        'meetingNumber' => $meeting_number ?? '',
        'password' => $meeting_password ?? '',
      ];

      // Attach the current user settings to drupalSettings.
      $elements['#attached']['drupalSettings']['user'] = [
        'userName' => $this->currentUser->getAccountName(),
        'userEmail' => $this->currentUser->getEmail(),
        'isAuthenticated' => $this->currentUser->isAuthenticated(),
      ];
    }

    return $elements;
  }

}
