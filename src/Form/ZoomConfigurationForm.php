<?php

namespace Drupal\zoom_video\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Zoom configuration form.
 */
class ZoomConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['zoom_video.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'zoom_configuration_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('zoom_video.settings');

    $form['zoom_app_credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Zoom APP Credentials'),
    ];

    $form['zoom_app_credentials']['sdk_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client ID'),
      '#default_value' => $config->get('sdk_key'),
    ];

    $form['zoom_app_credentials']['sdk_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Client Secret'),
      '#default_value' => $config->get('sdk_secret'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('zoom_video.settings');
    $config->set('sdk_key', $form_state->getValue('sdk_key'));
    $config->set('sdk_secret', $form_state->getValue('sdk_secret'));

    $config->save();

    parent::submitForm($form, $form_state);
  }

}
