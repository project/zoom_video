<?php

namespace Drupal\zoom_video;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Service to handle Zoom meeting SDK requests.
 */
class ZoomMeetingSDKRequest implements ZoomMeetingSDKRequestInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * ZoomMeetingSDKRequest constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function handleZoomMeetingRequest(Request $request): JsonResponse {
    $body = Json::decode($request->getContent());

    $config = $this->configFactory->get('zoom_video.settings');
    $zoomMeetingSDKKey = $config->get('sdk_key');
    $zoomMeetingSDKSecret = $config->get('sdk_secret');

    $iat = time() - 30;
    $exp = $iat + 60 * 60 * 2;

    $payload = [
      "sdkKey" => $zoomMeetingSDKKey,
      "mn" => $body["meetingNumber"],
      "role" => $body["role"],
      "iat" => $iat,
      "exp" => $exp,
      "appKey" => $zoomMeetingSDKKey,
      "tokenExp" => $iat + 60 * 60 * 2,
    ];

    if (empty($zoomMeetingSDKSecret)) {
      // Return an error response.
      throw new \Exception('Secret key is not set. Please config Zoom Meeting SDK secret key.');
    }

    $signature = $this->generateJwt($payload, $zoomMeetingSDKSecret);

    return new JsonResponse(["signature" => $signature]);
  }

  /**
   * {@inheritdoc}
   */
  public function generateJwt(array $payload, string $secret): string {
    return JWT::encode($payload, $secret, 'HS256');
  }

}
