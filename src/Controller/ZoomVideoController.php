<?php

namespace Drupal\zoom_video\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\zoom_video\ZoomMeetingSDKRequest;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines ZoomVideoController class.
 */
class ZoomVideoController implements ContainerInjectionInterface {

  /**
   * ZoomMeetingSDKRequest instance.
   *
   * @var \Drupal\zoom_video\ZoomMeetingSDKRequest
   */
  private $zoomSDK;

  /**
   * Logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private $logger;

  /**
   * Constructs a new ZoomVideoController.
   *
   * @param \Drupal\zoom_video\ZoomMeetingSDKRequest $zoomSDK
   *   The ZoomMeetingSDKRequest instance.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger instance.
   */
  public function __construct(ZoomMeetingSDKRequest $zoomSDK, LoggerInterface $logger) {
    $this->zoomSDK = $zoomSDK;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ZoomVideoController {
    return new static(
      $container->get('zoom_video.zoom_meeting_sdk_request'),
      $container->get('logger.factory')->get('zoom_video')
    );
  }

  /**
   * Fetches the handleZoomMeetingRequest functionalities and returns as JSON.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response from the Zoom SDK.
   *
   * @throws \Exception
   */
  public function fetchHandleZoomMeetingRequestFunctionalities(Request $request): JsonResponse {
    $response = $this->zoomSDK->handleZoomMeetingRequest($request);

    if ($response->getStatusCode() !== 200) {
      $this->logErrorResponse($response);
    }

    return $response;
  }

  /**
   * Logs the error response from the Zoom SDK.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response object.
   */
  private function logErrorResponse(Response $response) {
    $error = Json::decode($response->getContent());
    if (isset($error['error'])) {
      $this->logger->error('Zoom SDK error: {error}', ['error' => $error['error']]);
    }
    else {
      $this->logger->error('Unknown error occurred.');
    }
  }

}

