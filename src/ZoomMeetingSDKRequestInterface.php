<?php

namespace Drupal\zoom_video;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for Zoom meeting SDK request service.
 */
interface ZoomMeetingSDKRequestInterface {

  /**
   * Handles the POST request from Zoom.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function handleZoomMeetingRequest(Request $request): JsonResponse;

  /**
   * Generate JWT token.
   *
   * @param array $payload
   *   The JWT payload.
   * @param string $secret
   *   The JWT secret.
   *
   * @return string
   *   The generated JWT signature.
   */
  public function generateJwt(array $payload, string $secret): string;

}
