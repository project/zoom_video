# Zoom Video Module

Zoom Video Module manages the creation and handling of Zoom meetings on a 
Drupal website. It introduces a "Zoom Video" field type, allowing administrators 
to add meeting URLs to Drupal nodes. Drupal users can then join Zoom meetings 
directly from the website's pages. To interact with Zoom's services, the module 
relies on the Zoom API and SDK along with the Firebase PHP JWT library.

## Table of Contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Acknowledgment and Maintainers

## Requirements

- Drupal 10
- Firebase/PHP-JWT library
- HTTPS

## Installation

We recommend installing the module via Composer to ensure proper library 
installation. Once installed, enable the module in the Extend section of 
the Drupal admin dashboard.

## Configuration

1. Create a Zoom application by visiting the 
[Zoom App Marketplace](https://marketplace.zoom.us/) and selecting 
"Develop" from the top-right menu. Choose "General app" as the type of app you 
want to create. Next, select "User-managed app." From the "Build your app" menu 
in the left column, select the "Embed" tab and toggle on the "Meeting SDK" 
option. You can give your app a name by clicking the "Quick edit" pencil in  
the top left corner. Finally, copy the Client ID and Client Secret from 
“App Credentials” on the "Basic information" tab of the "Build your app" menu.

2. Navigate to Configuration > Development > Zoom Settings on your Drupal site 
and enter the Client ID and Client Secret.

3. In Structure > Content types, either select or create the content type for 
meetings and add a field of type “Zoom Video.”

4. Visit [zoom.us](https://zoom.us/) to create a new meeting. Click “Meetings” 
in the left menu, then “Schedule a meeting.” After saving, copy the meeting 
invite link. Start your meeting by clicking “Start” (you'll need to log into the 
Zoom application to complete this step).

5. Create new content on your Drupal website and paste the meeting invite link 
into the "Meeting URL" field.

6. Upon saving the content node, a "Join Meeting" button will be available to 
Drupal users with permission to view the node.

7. Adjust the meeting window's size by dragging the bottom right corner and 
reposition it by dragging it around the screen.

## Troubleshooting

- Ensure the Firebase/PHP-JWT library is correctly configured and the files are 
accessible.
- Check if the Zoom SDK JS library files are included on the page.
- Verify that your website is using HTTPS.

## FAQ

**Q:** What if I click "Join meeting" but it shows "The meeting hasn't started"?

**A:** Ensure the meeting's scheduled time has come, and the meeting has been 
started (not just scheduled) on Zoom.

**Q:** What if I click "Join meeting" but nothing happens?

**A:** Make sure the library is installed and configured correctly, and the
Drupal website is using HTTPS.

## Acknowledgment and Maintainers

Built within the Drupal community with assistance from Leonid Bogdanovych, aka 
[danmer](https://www.drupal.org/u/danmer) at ImageX. Many thanks to the 
community and all contributors. For questions or feedback, please open an 
issue in the [issue queue](https://www.drupal.org/project/issues/zoom_video).
